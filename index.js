const app = require('./app'); // подключаем главный файл app.js
const port = process.env.port || 5000; // объявление порта на котором будет запущен сервер

app.listen(port, () => console.log(`server successfully started on ${port}`)); // запуск сервера