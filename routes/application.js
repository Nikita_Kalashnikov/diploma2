const express = require('express');
const router = express.Router();
const controller = require('../controllers/application');

router.post('/', controller.createApplication);

module.exports = router;