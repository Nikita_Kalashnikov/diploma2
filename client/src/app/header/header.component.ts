import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'yasna-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public socialLinks: {path: string, imgPath: string}[] = [
    {
      path: 'https://www.facebook.com/beltelecomby',
      imgPath: '../../assets/images/fb.jpg'
    },
    {
      path: 'https://www.facebook.com/beltelecomby',
      imgPath: '../../assets/images/in.jpg'
    },
    {
      path: 'https://www.facebook.com/beltelecomby',
      imgPath: '../../assets/images/ok.jpg'
    },
    {
      path: 'https://www.facebook.com/beltelecomby',
      imgPath: '../../assets/images/tw.jpg'
    },
    {
      path: 'https://www.facebook.com/beltelecomby',
      imgPath: '../../assets/images/vk.jpg'
    }
  ]


  constructor() { }

  ngOnInit(): void {
  }

}
