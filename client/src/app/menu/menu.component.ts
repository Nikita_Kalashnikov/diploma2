import {Component} from '@angular/core';

@Component({
  selector: 'yasna-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent {

  constructor() { }

  public scrollTo(el):void {
    document.querySelector(el).scrollIntoView()
  }

}
