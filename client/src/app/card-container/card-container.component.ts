import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'yasna-card-container',
  templateUrl: './card-container.component.html',
  styleUrls: ['./card-container.component.scss']
})
export class CardContainerComponent implements OnInit {
  public cards = [
    {
      tariffName: 'Ясна',
      tariffPlan: '35',
      price: '33.50',
      description: 'Безлимитный интернет 35/17.5 Мбит/с и доступ в публичных сетях Wi-Fi («BELTELECOM», «byflyWIFI»)',
      tvStation: 'Интерактивное ТВ на 1 телеприемник',
      channelList: [
        {
          channelName: 'ОНТ',
          channelDescription: 'Канал долбоебов'
        },
        {
          channelName: 'НТВ',
          channelDescription: 'Канал кретинов'
        }
      ],
      additionalInfo: 'Безлимитные соединения по РБ в сети Белтелеком с услугой CLIP',
      cardColor: '#F4A92B',
      additionalName: 'плюс'
    },
    {
      tariffName: 'Ясна',
      tariffPlan: '35',
      price: '33.50',
      description: 'Безлимитный интернет 35/17.5 Мбит/с и доступ в публичных сетях Wi-Fi («BELTELECOM», «byflyWIFI»)',
      tvStation: 'Интерактивное ТВ на 1 телеприемник',
      channelList: [
        {
          channelName: 'ОНТ',
          channelDescription: 'Канал долбоебов'
        },
        {
          channelName: 'НТВ',
          channelDescription: 'Канал кретинов'
        }
      ],
      additionalInfo: 'Безлимитные соединения по РБ в сети Белтелеком с услугой CLIP',
      cardColor: '#F4A92B',
      additionalName: 'плюс'
    },
    {
      tariffName: 'Ясна',
      tariffPlan: '35',
      price: '33.50',
      description: 'Безлимитный интернет 35/17.5 Мбит/с и доступ в публичных сетях Wi-Fi («BELTELECOM», «byflyWIFI»)',
      tvStation: 'Интерактивное ТВ на 1 телеприемник',
      channelList: [
        {
          channelName: 'ОНТ',
          channelDescription: 'Канал долбоебов',
        },
        {
          channelName: 'НТВ',
          channelDescription: 'Канал кретинов',
        }
      ],
      additionalInfo: 'Безлимитные соединения по РБ в сети Белтелеком с услугой CLIP',
      cardColor: '#F4A92B',
      additionalName: 'плюс'
    }
  ]

  constructor() {
  }

  ngOnInit(): void {
  }

}
